import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Awal from '../screen/Awal'
import Info from '../screen/Info'
import Login from '../screen/Login'

const Stack = createNativeStackNavigator();
function index() {
return (
<NavigationContainer>
<Stack.Navigator>

<Stack.Screen name="Judul Film" component={Awal} />
<Stack.Screen name="Informasi Lengkap" component={Info} />
<Stack.Screen name="Login" component={Login} />

</Stack.Navigator>
</NavigationContainer>
);
}
export default index; 