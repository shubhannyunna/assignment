import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  View,
  StyleSheet,Image,
  Button,
  ScrollView
} from 'react-native';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };
  }

  async getMovies() {
    try {
      const response = await fetch(
        'http://www.omdbapi.com/?s=Captain America&apikey=997061b4',
      );
      const json = await response.json();
      this.setState({data: json.Search});
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({isLoading: false});
    }
  }

  componentDidMount() {
    this.getMovies();
  }

  render() {
    const {data, isLoading} = this.state;
    const {navigation} = this.props

    return (
      <View>
        <View style={styles.content}>
          {isLoading ? (
            <ActivityIndicator />
          ) : (
            <ScrollView>
            <FlatList
              data={data}
             keyExtractor={({ id }, index) => id}
              renderItem={({item}) => (
             
                <View style={{backgroundColor: 'pink',}}>
                  <View style={styles.separator}>
                    <View>
                     <Image source={{uri:item.Poster}} style={{width:400,height:150}}/> 
                     </View>
                    <View style={{flex: 1}}>
                      <Text style={{flex: 1}}>
                        {item.Title} ({item.Year}) {item.Type} 
                      </Text>
                     <Button title="Informasi Lengkap"
                          onPress={() => navigation.navigate('Informasi Lengkap', {link : item.imdbID})} />
                    </View>
                  </View>
                </View>
             
              )}
            />
            </ScrollView>

          )}
        
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    borderBottomWidth: 5,
    paddingBottom: 5,
    paddingTop: 5,
    
  }
});
