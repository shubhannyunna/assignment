import React, {Component} from 'react';
import {Text, View, StyleSheet, TextInput, Image, Linking} from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={{backgroundColor: 'grey', height: 700}}>
        <View style={{marginHorizontal: 45}}>
          <Text style={styles.head}>Layar Kaca21</Text>
          <View style={{marginTop: 150}}>
            <Text style={styles.text}>Sign Up</Text>
            <Image style={styles.image} />
          </View>
          <Text
            style={styles.text3}
            onPress={() => Linking.openURL('https://google.com')}>
            Forgot Password?
          </Text>
        </View>
      </View>
    )
}}

const styles = {
  head: {
    fontSize: 48,
    fontWeight: 'bold',
    color: '#00b894',
    paddingTop: 20,
    textAlign: 'center',
  },
  text1: {
    fontSize: 18,
    padding: 1,
    fontWeight: '700',
    color: 'white',
  },
  text: {
    fontSize: 18,
    textAlign: 'left',
    fontWeight: '700',
    color: 'white',
  },
  text3: {
    fontSize: 18,
    textAlign: 'left',
    fontWeight: '700',
    color: 'yellow',
  },
  textInput: {
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#C4C4C4',
    paddingHorizontal: 15,
  },
  btnLogin: {
    backgroundColor: 'red',
    borderRadius: 100,
    width: 300,
    height: 50,
    marginTop: 50,
    paddingHorizontal: 60,
    paddingVertical: 10,
  },
  textLogin: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
  },
  image: {
    height: 70,
    width: 70,
    borderRadius: 35,
  },
};